# Diplomacy Glossary

## https://diplomacy-things.gitlab.io/glossary/

## A resource for players of all tenures

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Contributing](#contributing)
- [GitLab CI](#gitlab-ci)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Contributing

### How to add, change, or delete terms

Change `data-enable-help-for-making-changes` to be `"true"` in `index.html` then open the page locally e.g. `python -m http.server`

Read the new section "How to Make Changes", if you've set everything up, go into the `public/` folder, then run
`sed -n '/START OF editor.js$/,$p' index.html | FILE=index.html node`
and use the "Create a new glossary item" button.

### After you have some changes

Reach out to `pyxxy` on Discord, or create an issue with your suggestions, or create a Merge Request with your changes.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for specific details.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

All HTML files must be in the `public/` directory to be served, but that artifact path is changeable.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

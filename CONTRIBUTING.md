<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Contributing](#contributing)
  - [Suggesting additions, changes, edits, or removals](#suggesting-additions-changes-edits-or-removals)
  - [Things to consider](#things-to-consider)
    - [Additions](#additions)
    - [Changes/Edits](#changesedits)
    - [Removals](#removals)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Contributing

## The aim of this project is:

  - To provide players with a resource for terms, phrases, abbreviations, etc. that they might not have heard before
  - To give newer players a resource to check without having to ask someone else
  - To exist as a resource even if owners and contributers change over time

## Suggesting additions, changes, edits, or removals

Please contact me -> `pyxxy#2826` on Discord with any suggestions! I'm fairly active in the [Nexus Leagues discord server](https://discord.gg/GeWhNMWeBC), if you want to tag me in there.

If you are technical, please feel free to create a Merge Request with your desired changes and add `pyxxy` as a reviewer.

Thank you for your contributions!

## Things to consider

### Additions

  - Is is a new term or a variation of an existing one?

### Changes/Edits

  - How is this improving the existing definition?
  - Is something missing? Could it be clearer?

### Removals

  - Why does the term need to be removed?
  - Can it be merged into another term?
  - Is it too vague? Is it too specific?
  - Is it not specific enough to Diplomacy?